'use strict';

class ProductService {
  constructor(app) {
    this.app = app;
  }

  list (offset = 0, limit = 20) {
    return this.app.models.Product
      .find()
      .skip(offset)
      .limit(limit);
  }

  findOneById(id) {
    return this.app.models.Product.findOne({ _id: id });
  }
}

module.exports = ProductService;
