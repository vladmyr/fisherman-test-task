'use strict';

const Promise = require('bluebird');

// randomly picked ObjectId that will
// represent shared among users cart
const STATIC_USER_ID = '507f191e810c19729de860ea';

class CartService {
  constructor(app) {
    this.app = app;
  }

  getState() {
    const Cart = this.app.models.Cart;

    return Promise.resolve().then(() => {
      return Cart
        .findOne({ userId: STATIC_USER_ID })
        .populate('lstProductId')
    }).then((cartInstance) => {
      if (cartInstance) {
        return cartInstance
      } else {
        return new Cart({
          userId: STATIC_USER_ID,
          lstProductIds: []
        });
      }
    })
  }

  toggleState (productId) {
    const Cart = this.app.models.Cart;

    return Promise.resolve().then(() => {
      return Cart.findOne({ userId: STATIC_USER_ID })
    }).then((cartInstance) => {
      cartInstance = cartInstance || new Cart({
          _id: STATIC_USER_ID,
          userId: STATIC_USER_ID,
          lstProductId: []
        });

      const index = cartInstance.lstProductId.indexOf(productId);

      if (index == -1) {
        cartInstance.lstProductId.push(productId);
      } else {
        cartInstance.lstProductId.splice(index, 1);
      }

      return cartInstance.save()
    }).then((cartInstance) => {
      return cartInstance.populate('lstProductId').execPopulate();
    })
  }

  checkout() {
    const Cart = this.app.models.Cart;

    return Promise.resolve().then(() => {
      return Cart.remove()
    }).then(CartService.getState);
  }
}

module.exports = CartService;
