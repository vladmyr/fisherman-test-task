'use strict';

const Promise = require('bluebird');
const faker = require('faker');

const PRODUCTS_COUNT = 10;

/**
 * Populate products collection
 * @param app
 */
const populateProducts = (app) => {
  let products = [];

  for (let i = 0; i < PRODUCTS_COUNT; i++) {
    products.push({
      name: faker.commerce.productName(),
      price: faker.commerce.price(),
      image: faker.image.image(),
      description: faker.lorem.sentence()
    })
  }

  return app.models.Product.insertMany(products);
};

module.exports = (app) => {
  return Promise.resolve().then(() => {
    return populateProducts(app);
  })
};

