'use strict';

const HTTP_STATUS_CODE = alias.require('@file.const.httpStatusCode');

const Promise = require('bluebird');
const _ = require('underscore');

const Util = alias.require('@file.helpers.util');


/**
 * Cart controller
 * @param   {express.Router}  router
 * @param   {Application}     app
 * @module
 */
module.exports = function(router, app){
  Util.Express.defineController({
    setup: function(){
      const self = this;

      router
        .get('/', self.getState, Util.Express.respondHandler)
        .post('/', self.bodyId, self.toggleProductState, Util.Express.respondHandler)
        .post('/checkout', self.checkout, Util.Express.respondHandler);

      router.path = 'cart';
    },

    bodyId(req, res, next) {
      const id = req.body.productId;

      if (app.mongoose.Types.ObjectId.isValid(id)) {
        return next();
      } else {
        return Util.Express.respond(res, HTTP_STATUS_CODE.BAD_REQUEST, 'Invalid parameter')
      }
    },

    /**
     * Get state of cart
     */
    getState(req, res, next) {
      return Promise
        .resolve()
        .then(() => {
          return app.services.CartService.getState();
        })
        .then((cart) => {
          req.setResponseBody(Util.Mongoose.toJSON(cart));
          return next();
        })
        .catch((e) => {
          res.setResponseError(e);
          return next();
        })
    },

    /**
     * Toggle in cart state
     */
    toggleProductState(req, res, next) {
      const id = req.body.productId;

      return Promise
        .resolve()
        .then(() => {
          return app.services.CartService.toggleState(id)
        })
        .then((cart) => {
          req.setResponseBody(Util.Mongoose.toJSON(cart));
          return next();
        })
        .catch((e) => {
          req.setResponseError(e);
          return next();
        })
    },

    /**
     * Checkout cart
     */
    checkout(req, res, next) {
      return Promise
        .resolve()
        .then(() => {
          return app.services.CartService.checkout()
        })
        .then((cart) => {
          req.setResponseBody(Util.Mongoose.toJSON(cart));
          return next();
        })
        .catch((e) => {
          req.setResponseError(e);
          return next();
        })
    }
  })
};