'use strict';

const HTTP_STATUS_CODE = alias.require('@file.const.httpStatusCode');

const Promise = require('bluebird');
const _ = require('underscore');

const Util = alias.require('@file.helpers.util');

/**
 * Products controller
 * @param   {express.Router}  router
 * @param   {Application}     app
 * @module
 */
module.exports = function(router, app){
  Util.Express.defineController({
    setup: function(){
      let self = this;

      router
        .get('/', self.getList, Util.Express.respondHandler)
        .get('/:id', self.getOneById, Util.Express.respondHandler);

      router.path = 'products';

      router.param('id', self.paramId);
    },

    paramId(req, res, next, id) {
      if (app.mongoose.Types.ObjectId.isValid(id)) {
        return next();
      } else {
        return Util.Express.respond(res, HTTP_STATUS_CODE.BAD_REQUEST, 'Invalid parameter')
      }
    },

    /**
     * Get multiple products
     */
    getList(req, res, next) {
      return Promise
        .resolve()
        .then(() => {
          return app.services.ProductService.list(req.query.offset, req.query.limit)
        })
        .then((lst) => {
          req.setResponseBody(Util.Mongoose.toJSON(lst));
          return next();
        })
        .catch((e) => {
          req.setResponseError(e);
          return next();
        })
    },

    /**
     * Get single product
     */
    getOneById(req, res, next) {
      return Promise
        .resolve()
        .then(() => {
          return app.services.ProductService.findOneById(req.params.id)
        })
        .then((item) => {
          req.setResponseBody(Util.Mongoose.toJSON(item));
          return next();
        })
        .catch((e) => {
          req.setResponseError(e);
          return next();
        })
    }
  })
};