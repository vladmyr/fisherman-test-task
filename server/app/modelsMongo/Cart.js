'use strict';

const _ = require('underscore');

const Cart = (define, defineSchema, SchemaTypes) => {
  return define ('cart', {
    userId: {
      type: SchemaTypes.ObjectId,
      unique: true
    },
    lstProductId: [{
      type: SchemaTypes.ObjectId,
      ref: 'product'
    }]
  }, {
    index: [{
      fields: {
        userId: 1
      },
      options: {
        name: 'userId',
        unique: true
      }
    }]
  })
};

module.exports = Cart;