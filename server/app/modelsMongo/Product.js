'use strict';

const _ = require('underscore');

const Product = (define, defineSchema, SchemaTypes) => {
  return define ('product', {
    name: String,
    price: String,
    image: String,
    description: String
  })
};

module.exports = Product;