## Fisherman Labs test task

### Description
A test task that was done for Fisherman Labs company

### Technological stack ###
Web application constists of separarte backend and frontend applications. 

#### Backend
- Written in `Javascript`
- Uses `Node.js` as an execution environment
- Features `Express` web application framework
- Features `Mongoose` DAO for Mongo database

#### Frontend 
- `React` as a view layer
- `Redux` in conjuction with `Immutable.js` for handling state
- `React-Router` for handling routing
- `Backbone.Model` and `Backbone.Collection` for hadnling HTTP daa communication with the server
- `Compass` stylesheet
- `Gulp` and `webpack` as building automation tools

#### Database
- `MongoDB`

### How to run?

#### Pre-requirements
- The development was performed using versions of dependencies as listed below:
	- `Node.js - v6.3.0`
	- `NPM - v3.10.3`
	- `MongoDB - 3.2.0`
- MongoDB database should be accessible on ip address `127.0.0.1:27017`
- Ports `8080` (for client) and `8081` (for web server) must be free

#### How to install?
Execute the following commands:
```cmd
git clone https://vladmyr@bitbucket.org/vladmyr/fisherman-test-task.git
cd fisherman-test-task
```

##### Server
Insire the root of just cloned repository execute:
```cmd
cd ./server
npm install
```

Next you need to populate your database with fake data. This will create a database `fishermanv1` and populate it:
```cmd
npm run db-populate
```

##### Client
You may also need to install webpack, webpack-dev-server and gulp globally and install all the dependencies. Insire the root of repository execute the following:
```cmd
cd ./server
npm install webpack webpack-dev-server gulp -g
npm install
```

Now you should be ready to run it.

#### How to run? ####
As for the server execute:
```cmd
cd ./server
node main.js
```
and for the clien execute:
```cmd
cd ./client
npm run dev
```
that will transpile and build project's files and run webpack-dev-server. Afer that you should be able to navigate to [http://localhost:8080/](http://localhost:8080/) to see the final result.