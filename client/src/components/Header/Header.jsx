'use strict';

import React from 'react';
import { connect } from 'react-redux';

class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  getActiveState(pathname) {
    return this.props.pathname == pathname ? 'active' : '';
  }

  render() {
    return <header className="navbar navbar-inverse navbar-static-top">
      <div className="container">
        <ul className="nav navbar-nav">
          <li className={ this.getActiveState('/') }>
            <a href="#/">Products</a>
          </li>
          <li className={ this.getActiveState('/cart') }>
            <a href="#/cart">Cart</a>
          </li>
        </ul>
      </div>
    </header>;
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    pathname: state.get('routing').locationBeforeTransitions.pathname
  }
};

const HeaderContainer = connect(mapStateToProps)(Header);

export { Header, HeaderContainer }