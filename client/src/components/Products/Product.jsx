'use strict';

import React from 'react';
import { connect } from 'react-redux';

import {
  fetch,
  select
} from 'src/redux/actions/productActions'

const defaultProps = {
  selected: {
    name: 'Loading...'
  }
};

class Product extends React.Component {
  componentWillMount() {
    const id = this.props.params.id;

    this.props.select(id);
    this.props.fetch(this.props.params.id);
  }

  render() {
    return <div className="row">
      <div className="col-xs-12">
        <h3>
          <a href="#/">Product List</a> - { this.props.selected.name }
        </h3>
        { this.props.isNetProcessing
          ? <span>Loading...</span>
          : <dl className="dl-horizontal">
              <dt>Name</dt>
              <dd>{this.props.selected.name}</dd>
              <dt>Description</dt>
              <dd>{this.props.selected.description}</dd>
            </dl>

        }
      </div>
    </div>
  }
}

Product.defaultProps = defaultProps;

const mapStateTopProps = (state, ownProps) => {
  return state.get('product').toJS();
};

const mapDispatchToProps = {
  fetch,
  select
};

const ProductContainer = connect(
  mapStateTopProps,
  mapDispatchToProps
)(Product);

export { Product, ProductContainer };