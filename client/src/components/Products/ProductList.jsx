'use strict';

import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';

import { fetchList } from 'src/redux/actions/productActions';
import { toggleProduct } from 'src/redux/actions/cartActions';

const defaultProps = {
  title: 'Product List',
  list: []
};

class ProductList extends React.Component {
  componentWillMount() {
    this.props.fetchList();
  }

  isProductInCart(product) {
    return this.props.cart.productIds.indexOf(product.id) != -1
  }

  toggleProduct(id) {
    return () => this.props.toggleProduct(id);
  }

  render() {
    return <div className="row">
      <div className="col-xs-12">
        { this.props.title
          ? <div className="product__header">
              <h3>{ this.props.title }</h3>
            </div>
          : null
        }
        <table className="table table-striped table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th className="product__cart-actions"></th>
            </tr>
          </thead>
          <tbody>
            {this.props.list
              ? this.props.list.map((item, index) => {
                const isInCart = this.isProductInCart(item);
                const btnClassName = classNames('btn btn--cart-action', {
                  'btn-success': !isInCart,
                  'btn-primary': isInCart
                });

                return <tr key={index}>
                  <td>{index + 1}</td>
                  <td>
                    <a href={`#/products/${item.id}`}>
                      {item.name}
                    </a>
                  </td>
                  <td className="product__cart-actions">
                    <button
                      className={btnClassName}
                      onClick={this.toggleProduct.bind(this)(item.id)}
                      disabled={this.props.cart.isNetProcessing}
                    >
                      { isInCart ? 'Remove' : 'Add' }
                    </button>
                  </td>
                </tr>
              })
              : null
            }
          </tbody>
        </table>
      </div>
    </div>;
  }
}

ProductList.defaultProps = defaultProps;

const mapStateToProps = (state, ownProps) => {
  return {
    title: ownProps.title,
    list: ownProps.list || state.getIn(['product', 'list']).toJS(),
    cart: {
      isNetProcessing: state.getIn(['cart', 'isNetProcessing']),
      productIds: state.getIn(['cart', 'productIds']).toJS()
    }
  }
};

const mapDispatchToProps = {
  fetchList,
  toggleProduct
};

const ProductListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductList);

export { ProductList, ProductListContainer };