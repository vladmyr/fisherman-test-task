'use strict';

import React from 'react';
import { connect } from 'react-redux';

import { ProductListContainer } from 'src/components/Products/ProductList';

import { checkout } from 'src/redux/actions/cartActions';

class Cart extends React.Component {
  render() {
    if (this.props.cart.productIds && this.props.cart.productIds.length) {
      return <div>
        <div className="row product__header">
          <div className="col-xs-8">
            <h3>Products in cart</h3>
          </div>
          <div className="col-xs-4">
            <button
              className="btn btn-success pull-right"
              disabled={this.props.cart.isNetProcessing}
              onClick={this.props.checkout.bind(this)}
            >
              Checkout now
            </button>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <ProductListContainer
              title=""
              list={this.props.cart.products}
            />
          </div>
        </div>
      </div>
    } else {
      return <div className="row">
        <div className="col-xs-12">
          <h3>Your cart is empty</h3>
          <p>Have a look at the variety of our <a href="#/">products</a> and get something nice.</p>
        </div>
      </div>
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    cart: state.get('cart').toJS(),
    ownProps
  }
};

const mapDispatchToProps = {
  checkout
};

const CartContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Cart);

export { CartContainer }