'use strict';

import React from 'react';
import { connect } from 'react-redux';
import './bundle.scss';

import {
  fetchState
} from 'src/redux/actions/cartActions';

import { HeaderContainer } from 'src/components/Header/Header'

class App extends React.Component {
  componentWillMount() {
    this.props.fetchCartState();
  }

  render() {
    return <div className="universe">
      <HeaderContainer />
      <div className="container content">
        {this.props.children}
      </div>
    </div>
  }
}

const mapStateToProps = (state, ownProps) => (ownProps);

const mapDispatchToProps = {
  fetchCartState: fetchState
};

const AppContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

export default AppContainer;