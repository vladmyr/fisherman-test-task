'use strict';

import { Promise } from 'bluebird';
import { push } from 'react-router-redux';

import Cart from 'src/backbone/models/Cart';

export const REQ_STATE = 'CART_REQ_STATE';
export const RES_STATE = 'CART_RES_STATE';
export const REQ_TOGGLE_PRODUCT = 'CART_REQ_TOGGLE_PRODUCT';
export const RES_TOGGLE_PRODUCT = 'CART_RES_TOGGLE_PRODUCT';
export const REQ_CHECKOUT = 'CART_REQ_CHECKOUT';
export const RES_CHECKOUT = 'CART_RES_CHECKOUT';



/** ACTION CREATORS */

export const reqState = () => ({
  type: REQ_STATE
});

export const resState = (data) => ({
  type: RES_STATE,
  data: data
});

export const reqToggleProduct = (id) => ({
  type: REQ_TOGGLE_PRODUCT,
  id: id
});

export const resToggleProduct = (data) => ({
  type: RES_TOGGLE_PRODUCT,
  data: data
});

export const reqCheckout = () => ({
  type: REQ_CHECKOUT
});

export const resCheckout = (data) => ({
  type: RES_CHECKOUT,
  data: data
});



/** THUNK ACTION CREATORS */

export const fetchState = () => {
  return (dispatch, getState) => {
    const cart = new Cart();

    dispatch(reqState());

    return cart
      .fetch()
      .then((result) => {
        dispatch(resState(result.obj.toJSON()))
      })
      .catch((e) => {
        // ToDo: implement error handling
        console.error(e);
      })
  }
};

export const toggleProduct = (id) => {
  return (dispatch, getState) => {
    const cart = new Cart();

    dispatch(reqToggleProduct(id));

    return cart
      .toggleProduct(id)
      .then((result) => {
        dispatch(resToggleProduct(result.obj.toJSON()));
      })
      .catch((e) => {
        // ToDo: implement error handling
        console.error(e);
      })
  }
};

export const checkout = () => {
  return (dispatch, getState) => {
    const cart = new Cart();

    dispatch(reqCheckout());

    return cart
      .checkout()
      .then((result) => {
        dispatch(resCheckout(result.obj.toJSON()));
        dispatch(push('/'));
        return null;
      })
      .catch((e) => {
        // ToDo: implement error handling
        console.error(e);
      })
  }
};