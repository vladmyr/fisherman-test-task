'use strict';

import { Promise } from 'bluebird';
import * as _ from 'underscore';

import ProductCollection from 'src/backbone/collections/ProductCollection';
import Product from 'src/backbone/models/Product';

export const REQ_MANY = 'PRODUCT_REQ_MANY';
export const RES_MANY = 'PRODUCT_RES_MANY';
export const REQ_ONE = 'PRODUCT_REQ_ONE';
export const RES_ONE = 'PRODUCT_RES_ONE';
export const SELECT = 'PRODUCT_SELECT';




/** ACTION CREATORS */

export const reqMany = () => ({
  type: REQ_MANY
});

export const resMany = (list) => ({
  type: RES_MANY,
  list: list
});

export const reqOne = (id) => ({
  type: REQ_ONE,
  id: id
});

export const resOne = (data) => ({
  type: RES_ONE,
  data: data
});

export const select = (id) => ({
  type: SELECT,
  id: id
});



/** THUNK ACTION CREATORS */

export const fetchList = () => {
  return (dispatch, getState) => {
    const productCollection = new ProductCollection();

    dispatch(reqMany());

    return productCollection
      .fetch()
      .then((result) => {
        return dispatch(resMany(result.obj.toJSON()));
      })
      .catch((e) => {
        // TODO: implement error handling
        console.error(e);
      });
  }
};

export const fetch = (id) => {
  return (dispatch) => {
    const product = new Product({ id: id });

    dispatch(reqOne(id));

    return product
      .fetch()
      .then((result) => {
        dispatch(resOne(result.obj.toJSON()));
      })
      .catch((e) => {
        // ToDo: implement error handling
        console.error(e);
      })
  }
};