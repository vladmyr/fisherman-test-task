import { combineReducers } from 'redux';
import { Map } from 'immutable';
import { routerReducer } from 'react-router-redux';

import { productReducer } from './productReducer';
import { cartReducer } from './cartReducer';

const reducers = (state = Map(), action) => {
  return Map({
    routing: routerReducer(state.get('routing'), action),
    product: productReducer(state.get('product'), action),
    cart: cartReducer(state.get('cart'), action)
  })
};

export default reducers;