'use strict';

import { Map, List, fromJS } from 'immutable';
import * as _ from 'underscore';
import * as actions from 'src/redux/actions/productActions';

const initialState = Map({
  isNetProcessing: false,
  list: List()
});

const findOneById = (list, id) => {
  const index = _.pluck(list.toJS(), 'id').indexOf(id);

  if (index == -1) {
    return;
  } else {
    return list.get(index).toJS();
  }
};

const setIsNetProcessing = (state, bool = false) => {
  return state.set('isNetProcessing', bool);
};

const setList = (state, list = []) => {
  return state.set('list', fromJS(list));
};

const select = (state, id) => {
  const product = findOneById(state.get('list'), id);

  return state.set('selected', Map({
    id: id,
    name: product.name
  }));
};

const resOne = (state, product) => {
  return state.set('selected', fromJS(product));
};

const productReducer = (state = initialState, action) => {
  switch(action.type) {
    case actions.REQ_MANY:
      return setIsNetProcessing(state, true);
    case actions.RES_MANY:
      return setIsNetProcessing(setList(state, action.list));
    case actions.REQ_ONE:
      return setIsNetProcessing(state, true);
    case actions.RES_ONE:
      return setIsNetProcessing(resOne(state, action.data));
    case actions.SELECT:
      return select(state, action.id);
    default:
      return state;
  }
};

export { productReducer, initialState };