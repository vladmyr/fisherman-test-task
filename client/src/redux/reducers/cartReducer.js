'use strict';

import { Map, List, fromJS } from 'immutable';
import * as _ from 'underscore';
import * as actions from 'src/redux/actions/cartActions';

const initialState = Map({
  isNetProcessing: false,
  products: List(),
  productIds: List()
});

const setIsNetProcessing = (state, bool = false) => {
  return state.set('isNetProcessing', bool);
};

const resState = (state, data) => {
  const productIds = _.pluck(data.lstProductId, 'id');
  return state
    .set('productIds', fromJS(productIds))
    .set('products', fromJS(data.lstProductId));
};

const cartReducer = (state = initialState, action) => {
  switch(action.type) {
    case actions.REQ_STATE:
    case actions.REQ_TOGGLE_PRODUCT:
    case actions.REQ_CHECKOUT:
      return setIsNetProcessing(state, true);
    case actions.RES_STATE:
    case actions.RES_TOGGLE_PRODUCT:
    case actions.RES_CHECKOUT:
      return setIsNetProcessing(resState(state, action.data));
    default:
      return state;
  }
};

export { cartReducer, initialState };