'use strict';

import BaseCollection from './BaseCollection';
import Util from 'src/helpers/util/index';
import Product from 'src/backbone/models/Product';

const name = 'ProductCollection';
const ProductCollection = BaseCollection.extend(Util.Collection.postDeclaration({
  model: Product
}, name));

export default ProductCollection;