'use strict';

import Util from 'src/helpers/util/index';
import BaseModel from './BaseModel';

const name = 'Product';
const Product = BaseModel.extend(Util.Model.postDeclaration({}, name));

export default Product;