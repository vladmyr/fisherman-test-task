'use strict';

import * as url from 'url';

import Util from 'src/helpers/util/index';
import BaseModel from './BaseModel';

const name = 'Cart';
const Cart = BaseModel.extend(Util.Model.postDeclaration({
  toggleProduct(id) {
    return this.save({
      productId: id
    }, {
      type: 'POST'
    })
  },

  checkout() {
    this.url = Util.Model.getUrl(name, 'pathnameCheckout');
    return this.save({}, {
      type: 'POST'
    });
  }

}, name));

export default Cart;