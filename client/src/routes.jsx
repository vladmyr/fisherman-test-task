import React from 'react';

import App from 'src/components/App/App';
import { ProductListContainer } from 'src/components/Products/ProductList';
import { ProductContainer } from 'src/components/Products/Product';
import { CartContainer } from 'src/components/Cart/Cart';

const routes = {
  path: '/',
  component: App,
  indexRoute: {
    component: ProductListContainer
  },
  childRoutes: [{
    path: '/products/:id',
    component: ProductContainer
  },{
    path: '/cart',
    component: CartContainer
  }]
};

export default routes;