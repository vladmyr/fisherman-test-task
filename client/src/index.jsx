'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, hashHistory} from 'react-router';
import {Provider} from 'react-redux';
import {syncHistoryWithStore} from 'react-router-redux';

import * as config from './config.json';
import routes from './routes';
import configureStore from './redux/store/configureStore';

const createSelectLocationState = () => {
  return (state) => {
    return state.get('routing')
  }
};

/** app redux store */
const store = configureStore();
const history = syncHistoryWithStore(hashHistory, store, {
  selectLocationState: createSelectLocationState()
});

/** app initial render */
ReactDOM.render(
  <Provider store={store}>
    <Router routes={routes} history={history} />
  </Provider>,
  document.getElementById('app')
);